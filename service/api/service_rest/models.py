from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Technician(models.Model):
    first_name = models.CharField(max_length=150, null=True)
    last_name = models.CharField(max_length=150, null=True)
    employee_id = models.PositiveSmallIntegerField(null=True, unique=True)

    def get_api_url(self):
        return reverse("api_show_technician", kwargs={"pk": self.pk})


class Appointment(models.Model):
    date_time = models.DateTimeField(auto_now=False, auto_now_add=False, null=True)
    reason = models.CharField(max_length=150)
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=150, null=True)
    status = models.CharField(max_length=50, null=True)
    technician = models.ForeignKey(
        Technician, related_name="appointments", on_delete=models.CASCADE
    )

    def finish(self):
        self.status = 'finished'
        self.save()

    def cancel(self):
        self.status = 'canceled'
        self.save()

    def get_api_url(self):
        return reverse("api_show_presentation", kwargs={"pk": self.pk})

    def __str__(self):
        return self.title
