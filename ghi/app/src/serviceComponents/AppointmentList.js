import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Appointment from './Appointment';

function AppointmentList() {

  const [appointments, setAppointments] = useState([]);

  useEffect(() => {
    async function fetchAppointments() {
      try {
        const response = await fetch("http://localhost:8080/api/appointments/");
        if (!response.ok) {
          console.error("Error loading appointments data");
        } else {
          const data = await response.json();
          setAppointments(data.appointments);
        }
      } catch (error) {
        console.error(error);
      }
    }
    fetchAppointments();
  }, []);



  function removeAppt(id){
    const newList = appointments.filter(appt=>appt.id !== id);
    setAppointments(newList)
  }

  return (
    <div>
      <h2>Appointments List</h2>
      <Link to="/service/appointment/new" className="btn btn-primary">Create New Appointments</Link>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Date</th>
            <th>Time</th>
            <th>VIN</th>
            <th>Customer</th>
            <th>Reason</th>
            <th>Assigned Tech</th>
            <th>Status</th>
            <th>Update Status</th>
          </tr>
        </thead>
        <tbody>
          {appointments.filter(app=>app.status!=='canceled' && app.status!=='finished')
          .map(appointment => {
            return(<Appointment appt={appointment} key={appointment.id} removeAppt={removeAppt}/>)
          })}
        </tbody>
      </table>
    </div>
  );
}

export default AppointmentList;
