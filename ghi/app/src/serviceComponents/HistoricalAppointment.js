
function HistoricalAppointment({appt}){

    return(
        <tr key={appt.id}>
            <td>{appt.vin}</td>
            <td></td>
            <td>{appt.customer}</td>
            <td>{(new Date(appt.date_time).toLocaleDateString())}</td>
            <td>{(new Date(appt.date_time)).toLocaleTimeString('us', {timeStyle:"short"})}</td>
            <td>{`${appt.technician.first_name} ${appt.technician.last_name}`}</td>
            <td>{appt.reason}</td>
            <td>{appt.status}</td>
        </tr>
    )
}

export default HistoricalAppointment
