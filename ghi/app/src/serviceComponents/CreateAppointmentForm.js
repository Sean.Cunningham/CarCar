import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";


function CreateAppointmentForm(){

    const navigate = useNavigate();

    const [technicians, setTechnicians] = useState([]);
    const [dateTime, setDateTime] = useState('');
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [reason, setReason] = useState('');
    const [technician, setTechnician] = useState('');

    const fetchData = async () => {
        const techUrl = "http://localhost:8080/api/technicians/"
        const techResponse = await fetch(techUrl)
        if(!techResponse.ok){
            console.error("error getting the technicians data")
        } else {
            const techData = await techResponse.json();
            for (let item of techData.technicians){
                setTechnicians((technicians)=>{return [...technicians, item]});
            }
        }
    }

    useEffect(()=>{
        fetchData();
    }, []);

    const handleDateTimeChange = (event) => setDateTime(event.target.value);
    const handleVinChange = (event) => setVin(event.target.value);
    const handleCustomerChange = (event) => setCustomer(event.target.value);
    const handleReasonChange = (event) => setReason(event.target.value);
    const handleTechnicianChange = (event) => setTechnician(event.target.value);


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.date_time = dateTime;
        data.customer = customer;
        data.vin = vin;
        data.reason = reason;
        data.technician = technician;

        const URL = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        }


        const response = await fetch(URL, fetchConfig);
        if(!response.ok){
            console.error('error adding an appointment');
        } else {


            setDateTime('');
            setVin('');
            setCustomer('');
            setReason('');
            setTechnician('')
            navigate('/service/appointments')
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a New Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                        <div className="form-floating mb-3">
                            <input type="datetime-local" onChange={handleDateTimeChange} value={dateTime} required name="dateTime" id="dateTime" className="form-control" />
                            <label htmlFor="dateTime">Appointment date and time</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange} value={vin} placeholder="vin" required type="text" id="vin" className="form-control" name="vin" />
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleCustomerChange} value={customer} placeholder="customer" required type="text" id="customer" className="form-control" name="customer" />
                            <label htmlFor="customer">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <textarea onChange={handleReasonChange} value={reason} placeholder="reason" required id="reason" className="form-control" name="reason" ></textarea>
                            <label htmlFor="reason">Reason for Appointment</label>
                        </div>
                        <div>
                            <select onChange={handleTechnicianChange} value={technician} required id="technician" className="form-select" name="technician" multiple={false}>
                                <option value="">Technician</option>
                                    {technicians.map(tech=>{
                                    return (
                                        <option value={tech.id} key={tech.id}>{`${tech.first_name} ${tech.last_name}`}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );


}

export default CreateAppointmentForm;
