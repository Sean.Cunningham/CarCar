import FinishAppointmentButton from "./FinishAppointmentButton";
import CancelAppointmentButton from "./CancelAppointmentButton";
import { useState } from "react";



function Appointment({appt, handleChange, removeAppt}){
    const [status, setStatus] = useState(appt.status)

    async function handleCancel(id){
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel`, {method: "put"});
        if(!response.ok){
          console.error("error updating status to cancelled")
        } else {
          setStatus(["canceled"]);
          removeAppt(id)
        }
      }

      async function handleFinish(id){
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish`, {method: "put"});
        if(!response.ok){
          console.error("error updating status to finished")
        } else {
          setStatus(["finished"]);
          removeAppt(id);
        }
      }


    return(
        <tr key={appt.id} onChange={()=>handleChange(appt.id)}>
        <td>{(new Date(appt.date_time).toLocaleDateString())}</td>
        <td>{(new Date(appt.date_time)).toLocaleTimeString('us', {timeStyle:"short"})}</td>
        <td>{appt.vin}</td>
        <td>{appt.customer}</td>
        <td>{appt.reason}</td>
        <td>{`${appt.technician.first_name} ${appt.technician.last_name}`}</td>
        <td>{status}</td>
        <td><span><FinishAppointmentButton finish={handleFinish} id={appt.id}/></span><span><CancelAppointmentButton cancel={handleCancel} id={appt.id}/></span></td>
    </tr>
    )
}

export default Appointment
