import { useState } from "react";
import { useNavigate } from "react-router-dom";

function CreateTechnicianForm(){

    const navigate = useNavigate();

    const [firstName, setFirstName] = useState([]);
    const [lastName, setLastName] = useState('');
    const [employeeID, setEmployeeID] = useState('');

    const handleFirstNameChange = (event) => setFirstName(event.target.value);
    const handleLastNameChange = (event) => setLastName(event.target.value);
    const handleEmployeeIDChange = (event) => setEmployeeID(event.target.value);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeID;


        const techURL = "http://localhost:8080/api/technicians/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(techURL, fetchConfig);
        if(!response.ok){
            console.error('error creating a technician');
        } else {


            setFirstName('');
            setLastName('');
            setEmployeeID('');
            navigate('/service/technicians')
        }
    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a New Technician</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleFirstNameChange} value={firstName} placeholder="firstName" required type="text" id="firstName" className="form-control" name="firstName" />
                        <label htmlFor="firstName">First Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleLastNameChange} value={lastName} placeholder="lastName" required type="text" id="lastName" className="form-control" name="lastName" />
                        <label htmlFor="lastName">Last Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleEmployeeIDChange} value={employeeID} placeholder="employeeID" required type="number" id="employeeID" className="form-control" name="employeeID" />
                        <label htmlFor="employeeID">employee ID</label>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    );

}

export default CreateTechnicianForm;
