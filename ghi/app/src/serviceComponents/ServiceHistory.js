import React, { useState, useEffect } from 'react';
import HistoricalAppointment from './HistoricalAppointment';
import SearchByVinForm from './SearchByVinForm';


function ServiceHistory() {
  const [appointments, setAppointments] = useState([]);


  useEffect(() => {
    async function fetchAppointments() {
      try {
        const response = await fetch("http://localhost:8080/api/appointments/");
        if (!response.ok) {
          console.error("Error loading appointments data");
        } else {
          const data = await response.json();
          setAppointments(data.appointments);
        }
      } catch (error) {
        console.error(error);
      }
    }
    fetchAppointments();
  }, []);

  function filter(vin){
    const filterList = appointments.filter(appt=>vin===appt.vin)
    setAppointments(filterList)
  }

  return (
    <div>
      <h2>Service History</h2>
      <SearchByVinForm appointments={appointments} filter={filter}/>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Assigned Tech</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {appointments
          .map(appointment => {
            return(<HistoricalAppointment appt={appointment} key={appointment.id} />)
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceHistory;
