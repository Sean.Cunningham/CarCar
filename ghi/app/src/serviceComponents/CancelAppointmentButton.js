

function CancelAppointmentButton({id, cancel}) {
  return <button className="btn btn-danger" onClick={()=>{cancel(id)}}>Cancel</button>
}
export default CancelAppointmentButton
