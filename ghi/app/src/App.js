import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import CreateManufacturerForm from './inventoryComponents/CreateManufacturerForm';
import ManufacturerList from './inventoryComponents/ManufacturerList';
import CreateModelForm from './inventoryComponents/CreateVehicleModelForm';
import CreateAutomobileForm from './inventoryComponents/CreateAutomobileForm';
import AutomobileList from './inventoryComponents/AutomobileList';
import ModelList from './inventoryComponents/ModelList';
import CreateTechnicianForm from './serviceComponents/CreateTechnicianForm';
import SalespeopleList from './salesComponents/SalespeopleList';
import CreateSalesperson from './salesComponents/CreateSalesperson';
import CustomerList from './salesComponents/CustomerList';
import CreateCustomerForm from './salesComponents/CreateCustomer';
import TechnicianList from './serviceComponents/TechnicianList';
import AppointmentList from './serviceComponents/AppointmentList';
import CreateAppointmentForm from './serviceComponents/CreateAppointmentForm';
import RecordSaleForm from './salesComponents/RecordSaleForm';
import SalesList from './salesComponents/SalesList';
import SalespersonHistory from './salesComponents/SalesPersonHistory';
import ServiceHistory from './serviceComponents/ServiceHistory';

function App() {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="inventory/" >
            <Route path="manufacturers" element={<ManufacturerList  />} />
            <Route path="automobiles" element={<AutomobileList  />} />
            <Route path="models" element={<ModelList  />} />
          </Route>
          <Route path="new/">
            <Route path="manufacturer" element={<CreateManufacturerForm />} />
            <Route path="model" element={<CreateModelForm />} />
            <Route path="Automobile" element={<CreateAutomobileForm />} />
          </Route>
          <Route path="service/">
            <Route path="history" element={<ServiceHistory />} />
            <Route path="technicians" element={<TechnicianList />} />
            <Route path="technician/">
              <Route path="new" element = {<CreateTechnicianForm />} />
            </Route>
            <Route path="appointments" element={<AppointmentList />} />
            <Route path="appointment/">
              <Route path="new" element={<CreateAppointmentForm />} />
            </Route>
          </Route>
          <Route path="sales">
            <Route path="salespeople" element={<SalespeopleList />} />
            <Route path="salesperson" element={<CreateSalesperson />} />
            <Route path="customers" element={<CustomerList />} />
            <Route path="customer" element={<CreateCustomerForm />} />
            <Route path="sale" element={<RecordSaleForm />} />
            <Route path="sales" element={<SalesList />} />
            <Route path="salesperson/history" element={<SalespersonHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
