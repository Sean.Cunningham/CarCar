import { useState } from "react";
import { useNavigate } from "react-router-dom";

function CreateManufacturerForm(){
    const navigate = useNavigate()

    const [name, setName] = useState('');

    const handleNameChange = (event)=>{
        setName(event.target.value);
    }

    const handleSubmit = async (event)=>{
        event.preventDefault();
        const data = {}
        data.name = name;
        JSON.stringify(data)

        const ManufactureURL = "http://localhost:8100/api/manufacturers/";
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(ManufactureURL, fetchConfig);
        if(!response.ok){
            console.error('error loading manufacturer data');
        } else {
            setName('')
            navigate('/inventory/manufacturers')
        }
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a New Manufacturer</h1>
            <form onSubmit={handleSubmit} id="create-manufacturer-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="name" required type="text" id="name" className="form-control" name="name" />
                <label htmlFor="name">Manufacturer Name</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );

}

export default CreateManufacturerForm;
