import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function AutomobileList() {
  const [automobiles, setAutomobiles] = useState([]);

  useEffect(() => {
    async function fetchAutomobiles() {
      try {
        const response = await fetch("http://localhost:8100/api/automobiles/");
        if (!response.ok) {
          console.error("Error loading automobiles data");
        } else {
          const data = await response.json();
          setAutomobiles(data.autos);
        }
      } catch (error) {
        console.error(error);
      }
    }
    fetchAutomobiles();
  }, []);

  return (
    <div>
      <h2>Automobiles List</h2>
      <Link to="/new/automobile" className="btn btn-primary">Create New Automobile</Link>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Manufacturer</th>
            <th>Model</th>
            <th>Color</th>
            <th>Year</th>
            <th>Price</th>
            <th>Sold</th>
          </tr>
        </thead>
        <tbody>
          {automobiles.map(automobile => (
            <tr key={automobile.id}>
              <td>{automobile.vin}</td>
              <td>{automobile.model.manufacturer.name}</td>
              <td>{automobile.model.name}</td>
              <td>{automobile.color}</td>
              <td>{automobile.year}</td>
              <td>{automobile.price}</td>
              <td>{automobile.sold ? 'Yes' : 'No'}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default AutomobileList;
