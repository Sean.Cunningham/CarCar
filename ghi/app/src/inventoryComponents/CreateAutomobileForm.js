import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

function CreateAutomobileForm(){

    const navigate = useNavigate();

    const [models, setModels] = useState([]);
    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');
    const [model, setModel] = useState('');

    const fetchData = async () => {
        const url = "http://localhost:8100/api/models"
        const response = await fetch(url)
        if(!response.ok){
            console.error("error getting the models data")
        } else {
            const data = await response.json();
            for (let item of data.models){
                setModels((models)=>{return [...models, item]});
            }
        }
    }

    useEffect(()=>{
        fetchData();
    }, []);

    const handleColorChange = (event) => setColor(event.target.value);
    const handleYearChange = (event) => setYear(event.target.value);
    const handleVinChange = (event) => setVin(event.target.value);
    const handleModelChange = (event) => setModel(event.target.value);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = model;

        const autoURL = "http://localhost:8100/api/automobiles/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(autoURL, fetchConfig);
        if(!response.ok){
            console.error('error adding an automobile');
        } else {

            setColor('');
            setYear('');
            setVin('');
            setModel('');
            navigate('/inventory/automobiles')
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a New automobile</h1>
                    <form onSubmit={handleSubmit} id="create-automobile-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleColorChange} value={color} placeholder="color" required type="text" id="color" className="form-control" name="color" />
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleYearChange} value={year} placeholder="year" required type="number" id="year" className="form-control" name="year" />
                        <label htmlFor="year">Year</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleVinChange} value={vin} placeholder="vin" required type="text" id="vin" className="form-control" name="vin" />
                        <label htmlFor="vin">VIN</label>
                    </div>
                    {/* <div className="mb-3">
                        <select onChange={handleSoldChange} value={sold} required id="sold" className="form-select" name="sold" multiple={false}>
                            <option value={true} key="true">True</option>
                            <option value={false} key="false">False</option>
                        </select>
                    </div> */}
                    <div>
                        <select onChange={handleModelChange} value={model} required id="model" className="form-select" name="model" multiple={false}>
                            <option value="">Model</option>
                                {models.map(model=>{
                                return (
                                <option value={model.id} key={model.id}>{`${model.name}`}</option>
                                );
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    );

}

export default CreateAutomobileForm;
