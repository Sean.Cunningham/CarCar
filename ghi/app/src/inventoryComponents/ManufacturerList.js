import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function ManufacturerList() {
  const [manufacturers, setManufacturers] = useState([]);

  useEffect(() => {
    async function fetchManufacturers() {
      try {
        const response = await fetch("http://localhost:8100/api/manufacturers/");
        if (!response.ok) {
          console.error("Error loading manufacturers data");
        } else {
          const data = await response.json();
          setManufacturers(data.manufacturers);
        }
      } catch (error) {
        console.error(error);
      }
    }
    fetchManufacturers();
  }, []);

  return (
    <div>
      <h2>Manufacturers List</h2>
      <Link to="/new/manufacturer" className="btn btn-primary">Create New Manufacturer</Link>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map(manufacturer => (
            <tr key={manufacturer.id}>
              <td>{manufacturer.name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ManufacturerList;
