import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

function CreateModelForm(){
  const navigate = useNavigate();
    const [manufacturers, setManufacturers] = useState([]);
    const [name, setName] = useState('');
    const [pictureURL, setPictureURL] = useState('');
    const [manufacturer, setManufacturer] = useState();

    const fetchData = async () =>{
      const url = "http://localhost:8100/api/manufacturers"
      const response = await fetch(url);
      if(!response.ok){
          console.error("error getting location information")
      } else {
          const data = await response.json();
          for (let item of data.manufacturers){
              setManufacturers(manufacturers=>[...manufacturers, item]);
          }
      }
    }

    useEffect(()=>{
        fetchData();
    }, []);

    const handleNameChange = (event) => setName(event.target.value)
    const handlePictureURLChange = (event) => setPictureURL(event.target.value)
    const handleManufacturerChange = (event) => setManufacturer(event.target.value)

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.picture_url = pictureURL;
        data.manufacturer_id = parseInt(manufacturer);

        const modelURL = "http://localhost:8100/api/models/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(modelURL, fetchConfig);
        if(!response.ok){
            console.error('error adding a model');
        } else {
            setName('');
            setPictureURL('');
            setManufacturer('');
            navigate('/inventory/models')
        }
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a New Vehicle Model</h1>
            <form onSubmit={handleSubmit} id="create-model-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="name" required type="text" id="name" className="form-control" name="name" />
                <label htmlFor="name">Model Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePictureURLChange} value={pictureURL} placeholder="pictureURL" required type="url" id="pictureURL" className="form-control" name="pictureURL" />
                <label htmlFor="pictureURL">Model Picture URL</label>
              </div>
              <div className="mb-3">
                <select onChange={handleManufacturerChange} value={manufacturer} required id="manufacturer" className="form-select" name="manufacturer" multiple={false}>
                  <option value="">Manufacturer</option>
                  {manufacturers.map(manufacturer=>{
                    return (
                    <option value={manufacturer.id} key={manufacturer.id}>{`${manufacturer.name}`}</option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default CreateModelForm;
