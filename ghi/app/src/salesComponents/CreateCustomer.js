import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function CreateCustomerForm()
{
    const navigate = useNavigate();

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [address, setAddress] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');

    const handleFirstNameChange = (event) => setFirstName(event.target.value);
    const handleLastNameChange = (event) => setLastName(event.target.value);
    const handleAddressChange = (event) => setAddress(event.target.value);
    const handlePhoneNumberChange = (event) => setPhoneNumber(event.target.value);

    const handleSubmit = async (event) =>
    {
        event.preventDefault();
        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.address = address;
        data.phone_number = phoneNumber;

        const customerURL = "http://localhost:8090/api/customers/";
        const fetchConfig =
        {
            method: "post",
            body: JSON.stringify(data),
            headers:
            {
                "Content-Type": "application/json"
            }
        };

        try
        {
            const response = await fetch(customerURL, fetchConfig);
            if (!response.ok)
            {
                console.error('Error adding a customer');
            }
            else
            {
                const newCustomer = await response.json();
                setFirstName('');
                setLastName('');
                setAddress('');
                setPhoneNumber('');
                navigate('/sales/customers');
            }
        }
        catch (error) {
            console.error(error);
        }
    };
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a New Customer</h1>
            <form onSubmit={handleSubmit} id="create-customer-form">
              <div className="form-floating mb-3">
                <input
                onChange={handleFirstNameChange}
                value={firstName}
                placeholder="First Name"
                required
                type="text"
                id="firstName"
                className="form-control"
                name="firstName" />
                <label htmlFor="firstName">First Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                onChange={handleLastNameChange}
                value={lastName}
                placeholder="Last Name"
                required
                type="text"
                id="lastName"
                className="form-control"
                name="lastName" />
                <label htmlFor="lastName">Last Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                onChange={handleAddressChange}
                value={address}
                placeholder="Address"
                required
                type="text"
                id="address"
                className="form-control"
                name="address" />
                <label htmlFor="address">Address</label>
              </div>
              <div className="form-floating mb-3">
                <input
                onChange={handlePhoneNumberChange}
                value={phoneNumber}
                placeholder="Phone Number"
                required
                type="text"
                id="phoneNumber"
                className="form-control"
                name="phoneNumber" />
                <label htmlFor="phoneNumber">Phone Number</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default CreateCustomerForm;
