import React, { useState, useEffect } from "react";

function SalesList() {
  const [sales, setSales] = useState([]);
  useEffect(() => {
    async function fetchSales() {
      try {
        const response = await fetch("http://localhost:8090/api/sales/");
        if (!response.ok) {
          console.error("Error fetching sales");
        } else {
            const salesData = await response.json();
            setSales(salesData.sales);
      }

    } catch (error) {
        console.error(error);
      }
    }
    fetchSales();
  }, []);

  return (
    <div className="container mt-4">
      <h1>All Sales</h1>
      <table className="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Salesperson</th>
            <th>Customer</th>
            <th>Automobile VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map((sale) => (
            <tr key={sale.href}>
              <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name} - ${sale.salesperson.employee_id}`}</td>
              <td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
              <td>{sale.automobile.vin}</td>
              <td>{`$${sale.price}`}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default SalesList;
