import React, { useState, useEffect } from "react";




function RecordSaleForm()
{
    const [automobiles, setAutomobiles] = useState([]);
    const [salespersons, setSalespersons] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [selectedAutomobile, setSelectedAutomobile] = useState("");
    const [selectedSalesperson, setSelectedSalesperson] = useState("");
    const [selectedCustomer, setSelectedCustomer] = useState("");
    const [price, setPrice] = useState("");


    useEffect(() =>
    {
    async function fetchData()
    {
        const automobileResponse = await fetch(
            "http://localhost:8100/api/automobiles/?sold=false"
        );
        const salespersonResponse = await fetch(
            "http://localhost:8090/api/salespeople/"
        );
        const customerResponse = await fetch(
            "http://localhost:8090/api/customers/"
        );

        if (
            !automobileResponse.ok ||
            !salespersonResponse.ok ||
            !customerResponse.ok
        ) {
            throw new Error("Error loading data");
        }

        const automobileData = await automobileResponse.json();
        const salespersonData = await salespersonResponse.json();
        const customerData = await customerResponse.json();

        setAutomobiles(automobileData.autos);
        setSalespersons(salespersonData.salespeople);
        setCustomers(customerData.customers);
    }

    fetchData();
    }, []);

    const handleSelectedAutomobileChange = (event) => setSelectedAutomobile(event.target.value);
    const handleSelectedSalespersonChange = (event) => setSelectedSalesperson(event.target.value);
    const handleSelectedCustomerChange = (event) => setSelectedCustomer(event.target.value);
    const handlePriceChange = (event) => setPrice(event.target.value);

    const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
        data.automobile = selectedAutomobile;
        data.salesperson = selectedSalesperson;
        data.customer = selectedCustomer;
        data.price = price;

    const saleURL = "http://localhost:8090/api/sales/";
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
        "Content-Type": "application/json",
        },
    };
        const response = await fetch(saleURL, fetchConfig);
        if (!response.ok) {
        console.error("Error recording the sale");
        } else {
        const newSale = await response.json();

        setSelectedAutomobile("");
        setSelectedSalesperson("");
        setSelectedCustomer("");
        setPrice("");

        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Record a New Sale</h1>
                <form onSubmit={handleSubmit} id="record-sale-form">
                <div className="mb-3">
                    <label htmlFor="automobile">Select an Automobile:</label>
                    <select
                    onChange={handleSelectedAutomobileChange}
                    value={selectedAutomobile}
                    required
                    id="automobile"
                    className="form-select"
                    name="automobile"
                    multiple={false}
                    >
                    <option value="">Select an Automobile</option>
                    {Array.isArray(automobiles) &&
                        automobiles.map((auto) => (
                        <option value={auto.vin} key={auto.id}>
                            {`${auto.model.manufacturer.name} ${auto.model.name} - ${auto.vin}`}
                        </option>
                        ))}
                    </select>
                </div>
                <div className="mb-3">
                    <label htmlFor="salesperson">Select a Salesperson:</label>
                    <select
                    onChange={handleSelectedSalespersonChange}
                    value={selectedSalesperson}
                    required
                    id="salesperson"
                    className="form-select"
                    name="salesperson"
                    multiple={false}
                    >
                    <option value="">Select a Salesperson</option>
                    {Array.isArray(salespersons) &&
                        salespersons.map((salesperson) => (
                        <option value={salesperson.id} key={salesperson.id}>
                            {`${salesperson.first_name} ${salesperson.last_name} - ${salesperson.employee_id}`}
                        </option>
                        ))}
                    </select>
                </div>
                <div className="mb-3">
                    <label htmlFor="customer">Select a Customer:</label>
                    <select
                    onChange={handleSelectedCustomerChange}
                    value={selectedCustomer}
                    required
                    id="customer"
                    className="form-select"
                    name="customer"
                    multiple={false}
                    >
                    <option value="">Select a Customer</option>
                    {Array.isArray(customers) &&
                        customers.map((customer) => (
                        <option value={customer.id} key={customer.id}>
                            {`${customer.first_name} ${customer.last_name} - ${customer.phone_number}`}
                        </option>
                        ))}
                    </select>
                </div>
                <div className="form-floating mb-3">
                    <input
                    onChange={handlePriceChange}
                    value={price}
                    placeholder="Price"
                    required
                    type="number"
                    id="price"
                    className="form-control"
                    name="price"
                    />
                    <label htmlFor="price">Price</label>
                </div>
                <button className="btn btn-primary">Record Sale</button>
                </form>
            </div>
            </div>
        </div>
    );
}

export default RecordSaleForm;
