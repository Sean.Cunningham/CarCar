import React, { useState, useEffect } from 'react';

function SalespersonHistory() {
  const [salespeople, setSalespeople] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState('');
  const [salesHistory, setSalesHistory] = useState([]);

  useEffect(() => {
    async function fetchSalespeople() {
      try {
        const response = await fetch('http://localhost:8090/api/salespeople/');

        if (!response.ok) {
          console.error('Error fetching salespeople');
        } else {
          const data = await response.json();
          setSalespeople(data.salespeople);
        }
      } catch (error) {
        console.error(error);
      }
    }

    fetchSalespeople();
  }, []);

  const handleSalespersonChange = (event) => {
    setSelectedSalesperson(event.target.value);
  };

  const handleFetchSalesHistory = async () => {
    try {
      if (!selectedSalesperson) {
        setSalesHistory([]);
        return;
      }

      const response = await fetch(
        `http://localhost:8090/api/sales/?salesperson=${selectedSalesperson}`
      );

      if (!response.ok) {
        throw new Error('Error fetching sales history');
      }

      const data = await response.json();
      setSalesHistory(data.sales);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className="container mt-4">
      <h1>Salesperson History</h1>
      <div className="mb-3">
        <label htmlFor="salesperson">Select a Salesperson:</label>
        <select
          onChange={handleSalespersonChange}
          value={selectedSalesperson}
          required
          id="salesperson"
          className="form-select"
          name="salesperson"
        >
          <option value="">Select a Salesperson</option>
          {salespeople.map((salesperson) => (
            <option value={salesperson.id} key={salesperson.id}>
              {`${salesperson.first_name} ${salesperson.last_name} - ${salesperson.employee_id}`}
            </option>
          ))}
        </select>
      </div>
      <button onClick={handleFetchSalesHistory} className="brn btn-primary">Fetch Sales History</button>
      {salesHistory.length > 0 ? (
        <table className="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Salesperson</th>
              <th>Customer</th>
              <th>Automobile VIN</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {salesHistory.map((sale) => (
              <tr key={sale.href}>
                <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name} - ${sale.salesperson.employee_id}`}</td>
                <td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
                <td>{sale.automobile.vin}</td>
                <td>{`$${sale.price}`}</td>
              </tr>
            ))}
          </tbody>
        </table>
      ) : (
        <p>No sales history available for the selected salesperson.</p>
      )}
    </div>
  );
}

export default SalespersonHistory;
